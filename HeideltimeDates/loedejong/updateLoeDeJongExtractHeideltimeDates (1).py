from xtas.tasks import es_document, heideltime
from xtas.tasks.es import store_single
from celery import chain
from elasticsearch import Elasticsearch
from xtas.tasks import fetch
import xml.etree.ElementTree as etree
from lxml import html
from xtas.tasks.es import store_single
from pyes import *

def processResults(results):
	for i in range(len(results)):
		try:
			print "Current id is " + str(results[i]["_id"]) + " and current iteration " + str(i)
			if(results[i]["_id"] != "nl.vk.d.11b-2.3.6"):
				doc = es_document('loedejong', 'doc', results[i]["_id"], 'plain_textDated')
				doc = fetch(doc)
				#parsedRepresentation = html.fromstring(doc).text_content()
				parsedRepresentation = doc
				if (parsedRepresentation == ""):
					parsedRepresentation = "abc"
				dates = heideltime(parsedRepresentation)
				print "update follows"
				#es.update('loedejong', 'doc', results[i]["_id"], body={"doc":{"plain_text":parsedRepresentation}})
				store_single(dates, 'heideltime', 'loedejong', 'doc', results[i]["_id"])
				print "update finished"
		except Exception as error:
			print type(error), ":", error
			import traceback
			traceback.print_exc()
			return (0)

es = Elasticsearch(["http://qhp:qhp@zookst18.science.uva.nl:8009"])
totalDocs = es.search('loedejong', body={'query':{'match_all':{}}})['hits']['total']
print "no of documents", totalDocs
results = es.search('loedejong', body={'query':{'match_all':{}}, "from": 0, "size": totalDocs})['hits']['hits']
print "total results", len(results)
processResults(results)
