from xtas.tasks import es_document, heideltime
from xtas.tasks.es import store_single
from celery import chain
from elasticsearch import Elasticsearch
from xtas.tasks import fetch
import xml.etree.ElementTree as etree
from lxml import html
from xtas.tasks.es import store_single
from pyes import *
import re

def processResults(results):
	updatedDocuments = 0
	docsUpdated = []
	for i in range(len(results)):
		try:
			print "Current id is " + str(results[i]["_id"]) + " and current iteration " + str(i)
			doc = es_document('loedejong', 'doc', results[i]["_id"], 'content')
			doc = fetch(doc)
			if (doc == ""):
				parsedRepresentation = ""
			else:
				parsedRepresentation = html.fromstring(doc).text_content()
			#find year mentions
			parsedRepresentationWithProperDates = re.sub(r"'(\d\d)", r"19\1", parsedRepresentation)			
			if (parsedRepresentation != parsedRepresentationWithProperDates):
				updatedDocuments += 1
				docsUpdated.append(results[i]["_id"])
				#print "docsUpdated", docsUpdated

			print "update follows"
			es.update('loedejong', 'doc', results[i]["_id"], body={"doc":{"plain_textDated":parsedRepresentationWithProperDates}})
			print "update finished"
		except:
			print "ISSUE"
	print "updatedDocuments", updatedDocuments
	print "docsUpdated", docsUpdated

es = Elasticsearch(["http://qhp:qhp@zookst18.science.uva.nl:8009"])
totalDocs = es.search('loedejong', body={'query':{'match_all':{}}})['hits']['total']
print "no of documents", totalDocs
results = es.search('loedejong', body={'query':{'match_all':{}}, "from": 0, "size": totalDocs})['hits']['hits']
print "total results", len(results)
processResults(results)
