from xtas.tasks import es_document, heideltime
from xtas.tasks.es import store_single
from celery import chain
from elasticsearch import Elasticsearch
from xtas.tasks import fetch
import xml.etree.ElementTree as etree
from lxml import html
from xtas.tasks.es import store_single
from pyes import *

def processResults(results, currentStartIndex):
	for i in range(len(results)):
		try:
			#print "results[0]", results[i]
			print "Current id is " + str(results[i]["_id"]) + " and current start index " + str(currentStartIndex) + " and current iteration " + str(i)
			#f.write("Current id is " + str(results[i]["_id"]) + "\n")
			doc = es_document('nlwikidated', 'page', results[i]["_id"], 'text')
			doc = fetch(doc)
			#print "Document content", "|" + doc + "|"
			if ((doc.strip() == "") or (doc.strip().startswith("<!--") and doc.strip().endswith("-->"))):
				parsedRepresentation = "abc"
			else:
				parsedRepresentation = html.fromstring(doc).text_content()
			dates = heideltime(parsedRepresentation)
			print "dates identified", dates
			#f.write(" ".join(dates) + "\n")
			store_single(dates, 'heideltime', 'nlwikidated', 'page', results[i]["_id"])
		except Exception as error:
			print type(error), ":", error
			import traceback
			traceback.print_exc()
			return (0)

es = Elasticsearch(["http://qhp:qhp@zookst18.science.uva.nl:8009"])
#get total number of documents inside the nlwiki index
totalDocs = es.search('nlwikidated', body={'query':{'match_all':{}}})['hits']['total']
print "no of hits", totalDocs

# impossible to read all documents at once because they are way too many, use scroll instead!
currentStartIndex = 0
#f = open("WikiOutput", "w")
while (currentStartIndex <= totalDocs):
	results = es.search('nlwikidated', body={'query':{'match_all':{}}, "from": currentStartIndex, "size": 5000})['hits']['hits']
	print "total results", len(results)
	#f.write("total results" + str(len(results)))
	#print "first result is", results[0]
	print "Current index is", currentStartIndex
	#f.write("Current index is" + str(currentStartIndex))
	processResults(results, currentStartIndex)
	
	# do the processing
	currentStartIndex += 5000
	print "Newly updated current index is", currentStartIndex
	#f.write("Newly updated current index is" + str(currentStartIndex))
#f.close()
