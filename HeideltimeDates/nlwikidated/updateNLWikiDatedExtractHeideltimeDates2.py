from xtas.tasks import es_document, heideltime
from xtas.tasks.es import store_single
from celery import chain
from elasticsearch import Elasticsearch
from xtas.tasks import fetch
import xml.etree.ElementTree as etree
from lxml import html
from xtas.tasks.es import store_single
from pyes import *
import re

from joblib import Parallel, delayed

from mwp import mwp_parse, mwp_text

from mwlib.parser.nodes import (ArticleLink, ImageLink, Text, Section, Table, Ref)
from mwlib.siteinfo import get_siteinfo
from mwlib.uparser import parseString
from mwlib.templ.misc import DictDB
from mwlib.dummydb import DummyDB

def processResults(results, currentStartIndex):
	jobs = [delayed(processResult)(results[i]) for i in range(len(results))]
	return Parallel(n_jobs=6, verbose=10, backend="threading")(jobs)

class UnspecifiedNode(object):
    """Placeholder for the dispatcher table."""
    pass
	
class MyDB(DictDB, DummyDB):
    """Kurwa, DictDB has no getURL() and DummyDB no get_site_info"""

    def __init__(self, siteinfo=None):
        super(MyDB, self).__init__()

        self.siteinfo = siteinfo

def articlelink_text(node):
    try:
        text_node = node.children[0]
        text = text_node.caption
    except IndexError:
        text = node.target

    yield (text, )

def heading_text(node):
    for i in dispatch_text(node.children[0]):
        yield i

    yield ('\n',)

    for i in dispatch_text(node.children[1:]):
        yield i

def dispatch(node, table):
    # print "   ***", type(node), node.asText()[:25].encode('utf-8')
    for child in node:
        child_type = type(child)
        # print "     *", child_type, child.asText()[:25].encode('utf-8')
        fn = (table[child_type] if child_type in table else
              table[UnspecifiedNode])

        for i in fn(child):
            yield i


def dispatch_links(node):
    return dispatch(node, _dispatch_links)


def dispatch_text(node):
    return dispatch(node, _dispatch_text)
	
def ignore(node):
    return
    yield
	
def plaintext(node):
    text = node.caption
    yield (text,)
	
def articlelink(node):
    try:
        text_node = node.children[0]
        text = text_node.caption
    except IndexError:
        text = node.target

    yield (node.target, text)

_dispatch_text = {Text: plaintext,
                  Section: heading_text,
                  ArticleLink: articlelink_text,
                  ImageLink: ignore,
                  Table: ignore,
                  Ref: ignore,

                  UnspecifiedNode: dispatch_text
                  }

_dispatch_links = {ArticleLink: articlelink,
                   Table: ignore,
                   Ref: ignore,
                   ImageLink: ignore,

                   UnspecifiedNode: dispatch_links
                   }
	
def parse(text, db=None, siteinfo=None):
    """Parse MediaWiki text and return the parse tree."""
    if siteinfo is None:
        siteinfo = get_siteinfo("nl")
    if db is None:
        db = MyDB(siteinfo=siteinfo)

    return parseString(raw=text, title='', wikidb=db)

def processResult(result):
	try:
		#print "results[0]", results[i]
		#print "Current id is " + str(results[i]["_id"]) + " and current start index " + str(currentStartIndex) + " and current iteration " + str(i)
		#f.write("Current id is " + str(results[i]["_id"]) + "\n")
		doc = es_document('nlwikidated', 'page', result["_id"], 'text')
		doc = fetch(doc)
		#print "Document content", "|" + doc + "|"
		if ((doc.strip() == "")): # or (doc.strip().startswith("<!--") and doc.strip().endswith("-->"))):
			parsedRepresentation = "abc"
		else:			
			options = {'links': (dispatch_links, "\n"),
						'text': (dispatch_text, "")}
			dispatcher, sep = options["text"]
			data = doc.encode('ascii', 'ignore').decode('ascii')
			si = get_siteinfo("nl")
			res = parse(data)
			output = sep.join("_".join(e for e in i) for i in dispatcher(res))
			parsedRepresentation = re.sub(r"\((\d{4})(\d{4})\)", r"(\1-\2)", output)
		dates = heideltime(parsedRepresentation)
		print "dates identified", dates
		#f.write(" ".join(dates) + "\n")
		store_single(dates, 'heideltime', 'nlwikidated', 'page', result["_id"])
	except Exception as error:
		print type(error), ":", error
		import traceback
		traceback.print_exc()
		return (0)

es = Elasticsearch(["http://qhp:qhp@zookst18.science.uva.nl:8009"])
#get total number of documents inside the nlwiki index
totalDocs = es.search('nlwikidated', body={'query':{'match_all':{}}})['hits']['total']
print "no of hits", totalDocs

# impossible to read all documents at once because they are way too many, use scroll instead!
currentStartIndex = 0
#f = open("WikiOutput", "w")
while (currentStartIndex <= totalDocs):
	results = es.search('nlwikidated', body={'query':{'match_all':{}}, "from": currentStartIndex, "size": 5000})['hits']['hits']
	print "total results", len(results)
	#f.write("total results" + str(len(results)))
	#print "first result is", results[0]
	print "Current index is", currentStartIndex
	#f.write("Current index is" + str(currentStartIndex))

	processResults(results, currentStartIndex)
	
	# do the processing
	currentStartIndex += 5000
	print "Newly updated current index is", currentStartIndex
	#f.write("Newly updated current index is" + str(currentStartIndex))
#f.close()
