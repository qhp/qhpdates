#from xtas.tasks import es_document, heideltime
#from xtas.tasks.es import store_single
#from celery import chain
from elasticsearch import Elasticsearch
#from xtas.tasks import fetch
#import xml.etree.ElementTree as etree
#from lxml import html
#from xtas.tasks.es import store_single
from pyes import *
import datetime

def isValidDate(date):
	validDate = 1
	try:
		datetime.datetime.strptime(date, '%Y-%m-%d')
		if(date < '1933-01-01' or date >= '1950-01-01'):
			validDate = 0 
	except ValueError:
		validDate = 0
	return validDate

def isValidMonth(date):
	validMonth = 1
	try:
		datetime.datetime.strptime(date, '%Y-%m')
		if(date < '1933-01' or date >= '1950-01'):
			validMonth = 0
	except ValueError:
		validMonth = 0
	return validMonth

def isValidYear(date):
	validYear = 1
	try:
		datetime.datetime.strptime(date, '%Y')
		if(date < '1933' or date >= '1950'):
			validYear = 0
	except ValueError:
		validYear = 0
	return validYear

def convertToDate(date):
	year, month, day = map(int, date.split("-"))
	properDate = datetime.date(year, month, day)
	return properDate

def processResults(results, currentStartIndex):
	for i in range(len(results)):
		try:
			print "Current id is " + str(results[i]["_id"]) + " and current start index " + str(currentStartIndex) + " and current iteration " + str(i)
			dates = results[i]["_source"]["xtas_results"]["heideltime"]["data"]
			#print "Dates are ", dates
			yearMentions = []
			monthMentions = []
			dayMentions = []
			uncertainDates = []
			for date in dates:
				if(isValidDate(date)):
					completeDate = convertToDate(date)
					if(completeDate not in dayMentions):
						dayMentions.append(completeDate)
				elif(isValidMonth(date)):
					completeDate = convertToDate(date + "-01")
					if(completeDate not in monthMentions):
						monthMentions.append(completeDate)
				elif(isValidYear(date)):
					completeDate = convertToDate(date + "-01-01")
					if (completeDate not in yearMentions):
						yearMentions.append(completeDate)
				elif(((date.startswith("XXXX-")
                                        or (("-XX") in date and int(date[:4]) >= 1933 and int(date[:4]) < 1950 )
                                        or ((date.endswith("-WI") or date.endswith("-SU") or date.endswith("-FA") or date.endswith("-SP")) and int(date[:4]) >= 1933 and int(date[:4]) < 1950 ))
					or (date.startswith("PAST_REF"))
					or (date.startswith("PRESENT_REF"))
					or (date.startswith("FUTURE_REF")))
                                        and date not in uncertainDates):
					uncertainDates.append(date)

			print "Day mentions: ", sorted(dayMentions)
			print "Month mentions: ", sorted(monthMentions)
			print "Year mentions: ", sorted(yearMentions)
			print "Uncertain dates: ", sorted(uncertainDates)
			es.update('nlwikidated', 'page', results[i]["_id"], body={"doc":{"yearMentions2":sorted(yearMentions)}})
			es.update('nlwikidated', 'page', results[i]["_id"], body={"doc":{"monthMentions2":sorted(monthMentions)}})
			es.update('nlwikidated', 'page', results[i]["_id"], body={"doc":{"dayMentions2":sorted(dayMentions)}})
			es.update('nlwikidated', 'page', results[i]["_id"], body={"doc":{"uncertain_dates":sorted(uncertainDates)}})

		except Exception as error:
			print type(error), ":", error
			import traceback
			traceback.print_exc()
			return (0)

es = Elasticsearch(["http://qhp:qhp@zookst18.science.uva.nl:8009"])
totalDocs = es.search('nlwikidated', body={'query':{'match_all':{}}})['hits']['total']
print "no of documents", totalDocs

currentStartIndex = 0
#f = open("WikiOutput", "w")
while (currentStartIndex <= totalDocs):
	results = es.search('nlwikidated', body={'query':{'match_all':{}}, "from": currentStartIndex, "size": 5000})['hits']['hits']
	print "total results", len(results)
	#f.write("total results" + str(len(results)))
	#print "first result is", results[0]
	print "Current index is", currentStartIndex
	#f.write("Current index is" + str(currentStartIndex))
	processResults(results, currentStartIndex)
	
	# do the processing
	currentStartIndex += 5000
	print "Newly updated current index is", currentStartIndex
	#f.write("Newly updated current index is" + str(currentStartIndex))
#f.close()
