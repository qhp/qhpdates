from xtas.tasks import es_document, heideltime
from xtas.tasks.es import store_single
from celery import chain
from elasticsearch import Elasticsearch
from xtas.tasks import fetch
import xml.etree.ElementTree as etree
from lxml import html
from xtas.tasks.es import store_single
from pyes import *

INDEX_NAME = 'nlwikidated'

def index_name(doc):
    return INDEX_NAME

def index_document(doc_obj, _id):
    index = index_name(doc_obj)
    if index:
        es.index(index,"page",doc_obj, id=_id)

def indexDocuments(results):
	for i in range(len(results)):
		try:
			print "Current id is " + str(results[i]["_id"]) + " and current iteration " + str(i)
			doc_obj = {}
			doc_obj["title"] = results[i]["_source"]["title"]
			doc_obj["text"] = results[i]["_source"]["text"]
			doc_obj["redirect"] = results[i]["_source"]["redirect"]
			doc_obj["redirect_page"] = results[i]["_source"]["redirect_page"]
			doc_obj["special"] = results[i]["_source"]["special"]
			doc_obj["stub"] = results[i]["_source"]["stub"]			
			doc_obj["disambiguation"] = results[i]["_source"]["disambiguation"]
			doc_obj["firstdate"] = results[i]["_source"]["firstdate"]			
			doc_obj["seconddate"] = results[i]["_source"]["seconddate"]
			doc_obj["category"] = results[i]["_source"]["category"]
			doc_obj["link"] = results[i]["_source"]["link"]

			index_document(doc_obj, results[i]["_id"])		

			"""
			doc = es_document('loedejong', 'doc', results[i]["_id"], 'plain_text')
			doc = fetch(doc)
			#parsedRepresentation = html.fromstring(doc).text_content()
			parsedRepresentation = doc
			if (parsedRepresentation == ""):
				parsedRepresentation = "abc"
			dates = heideltime(parsedRepresentation)
			print "update follows"
			#es.update('loedejong', 'doc', results[i]["_id"], body={"doc":{"plain_text":parsedRepresentation}})
			store_single(dates, 'heideltime', 'loedejong', 'doc', results[i]["_id"])
			print "update finished"
			"""
		except Exception as error:
			print type(error), ":", error
			import traceback
			traceback.print_exc()
			return (0)

es = Elasticsearch(["http://qhp:qhp@zookst18.science.uva.nl:8009"])
#totalDocs = es.search('nlwikinew', body={'query':{'query_string':{'query': 'text:%7B1933%20TO%201949%7D'}},"size":0})['hits']['total']
totalDocs = es.search('nlwikinew', body={'query':{"filtered":{"query":{"match_all":{}},"filter":{"range":{"text":{"from":"1933-01-01T00:00:00.000Z","to":"1949-12-31T23:59:00.041Z"}}}}},"size":0})['hits']['total']
print "no of documents", totalDocs

currentStartIndex = 0
while (currentStartIndex <= totalDocs):
	results = es.search('nlwikinew', body={'query':{"filtered":{"query":{"match_all":{}},"filter":{"range":{"text":{"from":"1933-01-01T00:00:00.000Z","to":"1949-12-31T23:59:00.041Z"}}}}}, "from": currentStartIndex, "size": 5000})['hits']['hits']
	print "total results", len(results)
	print "Current index is", currentStartIndex
	indexDocuments(results)
	currentStartIndex += 5000
	print "Newly updated current index is", currentStartIndex
